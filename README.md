# Болванка автономного скрипта.
Болванка автономного скрипта для [RutonyChat][RutonyChat].

**31/01/2023**
* Исправлена ошибка с не верным индексом массива.


**05/04/2022**
* Добавлена работа с коммандами, получаемыми из чата.
* Добавлена поддержка языков.




* * *
### Просмотреть все [мои работы в мастерской][myworkshopfiles].

* * *
### Как можно отблагодарить:
* Оформить удобную для вас подписку на [Boosty.to][Boosty]
* Разово поддержать через [DonationAlerts][DA]

[//]: # (Short links)

   [RutonyChat]: <https://store.steampowered.com/app/524660/RutonyChat/>
   [myworkshopfiles]: <https://steamcommunity.com/id/LLIaMMaH/myworkshopfiles/>
   [Boosty]: <https://boosty.to/lliammah/ref>
   [DA]: <https://www.donationalerts.com/r/lliammah>