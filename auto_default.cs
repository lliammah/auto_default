using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.IO;

/*****************************************************************************************
 * xx/xx/xxxx - 0.1 - Какое-то описание.
 *****************************************************************************************
 * Как можно отблагодарить:
 * Оформить удобную для вас подписку на Boosty.to - https://boosty.to/lliammah/ref
 * Разово поддержать через DonationAlerts - https://www.donationalerts.com/r/lliammah
*****************************************************************************************/


namespace RutonyChat
{
    public class ScriptName
    {

        public ScriptInfo si = new ScriptInfo();
        public DebugMode debug = new DebugMode();
        public Config config = new Config();

        private const int lngEN = 1;
        private const int lngRU = 2;
        private int Language = 0;

        public const int MSG_DANGER = 1;
        public const int MSG_WARNING = 2;
        public const int MSG_INFO = 3;

        public const string COMMAND_START = "!";

        public string[] cmd_ru = new string[] { "Команда1", "Команда2", "Команда3" };
        public string[] cmd_en = new string[] { "Command1", "Command1", "Command1" };
        public string choise_text = "";
        public string msg_text = "";

        // Строка из чата с командами и параметрами.
        private string[] _cmdString;

        // Для патернов в регулярках.
        private string _patt = "";

        readonly RegexOptions options = RegexOptions.IgnoreCase | RegexOptions.Singleline | RegexOptions.CultureInvariant;
        Regex regex;
        MatchCollection m_param;

        public void InitParams(Dictionary<string, string> param)
        {
            // Включен или нет режим отладки
            if (param.ContainsKey("DebugMode"))
            {
                try
                {
                    debug.Mode = bool.Parse(param["DebugMode"]);
                }
                catch { }
            }

            // Уровень отладки
            if (param.ContainsKey("DebugLevel"))
            {
                try
                {
                    var sTmp = param["DebugLevel"];
                    if (!string.IsNullOrEmpty(sTmp)) debug.Level = int.Parse(sTmp[0].ToString());
                }
                catch { }
            }

            // БОТЫ
            if (param.ContainsKey("BotList"))
            {
                try
                {
                    config.BotsList = StringSplit(param["BotList"]);
                }
                catch { }
            }

            // АДМИНЫ
            if (param.ContainsKey("CheckAdmin"))
            {
                try { config.CheckAdmin = bool.Parse(param["CheckAdmin"]); }
                catch { }
            }
            // Список ников админов
            if (param.ContainsKey("AdminList"))
            {
                try
                {
                    config.AdminList = StringSplit(param["AdminList"]);
                }
                catch { }
            }

            // ЧЁРНЫЙ СПИСОК
            if (param.ContainsKey("BlackNiks"))
            {
                try
                {
                    config.BlackList = StringSplit(param["BlackNiks"]);
                }
                catch { }
            }

            ///////////////////////////////////////////////////////////////////////////////
            // Если скрипту нужно хранить или читать какие-нибудь данные
            // Если мы работаем из папки воркшопа
            string folder = RutonyBotFunctions.GetScriptDirectory(si.Script);
            // Иначе мы работаем из папки стима
            if (folder == "") folder = ProgramProps.dir_scripts;
            if (!folder.EndsWith("\\")) folder += @"\";
            folder += si.Folder + @"\";

            if (CheckDir(folder + @"Data")) config.FolderData = folder + @"Data\";
            if (debug.Mode && debug.Level >= 2) Write_Log(MSG_INFO, "Рабочая директория", config.FolderData);

            RutonyBot.SayToWindow(string.Format(si.Info, si.Name, si.Version, ""));
        }

        public void Closing()
        {
            RutonyBot.SayToWindow(string.Format(si.Info, si.Name, si.Version, "dis"));
        }


        // Заглушка для стандартного метода
        public void NewAlert(string site, string typeEvent, string subplan, string name, string text, float donate, string currency, int qty) { }



        public void NewMessage(string site, string name, string text, bool system)
        {
            if (system) return;
            if (text[0] != '!') return;
			if (text.StartsWith(COMMAND_START)) return;

            if (IsName(config.BotsList, name, "Проверяем ботов"))
            {
                if (debug.Mode && debug.Level >= 2) Write_Log(MSG_INFO, "Bot", name);
                return;
            }

            if (!config.CheckdAdmin)
            {
                if (IsName(config.AdminList, name, "Проверяем админов"))
                {
                    if (debug.Mode && debug.Level >= 2) Write_Log(MSG_INFO, "Admin", name);
                    return;
                }
            }

            if (IsName(config.BlackList, name, "Проверяем чёрный список"))
            {
                if (debug.Mode && debug.Level >= 2) Write_Log(MSG_INFO, "Black Nick", name);
                return;
            }

            _cmdString = text.Trim().ToLower().Split(' ');

            // Основная команда для скрипта.
            _patt = @"^[!]{1}(?<cmdScript>";
            _patt += GenPattern(cmd_ru) + "|";
            _patt += GenPattern(cmd_en) + ")";

            regex = new Regex(_patt, options);
            var mOpen = regex.Matches(_cmdString[0]);

            if (debug.Mode && debug.Level >= 2) Write_Log(MSG_INFO, "Параметров", mOpen.Count.ToString());

            // Не нашли нужной команды - выход
            if (mOpen.Count == 0) return;

            // На каком языке выводить ответы.
            choise_text = _cmdString[0].Replace("!", "").Trim().ToLower();
            if (Array.Exists(cmd_en, v => v.ToLower() == choise_text)) Language = lngEN;
            else Language = lngRU;

            // if (Language == lngEN) msg_text = "English";
            // else msg_text = "Русский";



            // Далее наш скрипт
        }





        ////////////////////////////////////////////////////////////////////////////////////////////////////
        // Собираем строку с паттерном для команд
        public string GenPattern(string[] choices)
        {
            string res = "";
            for (int i = 0; i < choices.Length; i++)
            {
                res += choices[i].ToLower();
                if (i < choices.Length - 1) res += "|";
            }
            return res;
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        /// <summary>
        /// Возвращает слова в падеже, зависимом от заданного числа.
        /// </summary>
        /// <param name="i">Число от которого зависит выбранное слово.</param>
        /// <param name="s1">Именительный падеж слова. Например "день".</param>
        /// <param name="s2">Родительный падеж слова. Например "дня".</param>
        /// <param name="s3">Множественное число слова. Например "дней".</param>
        /// <returns></returns>
        public string DeclinationString(int i, string s1, string s2, string s3)
        {
            var titles = new[] { s1, s2, s3 };
            var cases = new[] { 2, 0, 1, 1, 1, 2 };
            return titles[i % 100 > 4 && i % 100 < 20 ? 2 : cases[(i % 10 < 5) ? i % 10 : 5]];
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////
        // Есть ли ник в нужном нам списке
        public bool IsName(string[] names, string name, string desc = "")
        {
            string fnc_title = "Поиск имени в массиве";
            var n = name.Trim().ToLower();
            if (debug.Mode && debug.Level >= 2)
            {
                Write_Log(MSG_INFO, "Что проверяем: <span style='color:#FFFF00;'>" + desc + "</span> / Размер массива: <span style='color:#FFFF00;'>" + names.Length.ToString() + "</span>");
                Write_Log(MSG_INFO, fnc_title, name);
            }
            if (names.Length == 0)
            {
                return false;
            }
            else
            {
                return names.Any(Nik => string.Equals(n, Nik.Trim().ToLower(), StringComparison.CurrentCultureIgnoreCase));
            }
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////
        // Поделим строку
        public string[] StringSplit(string str)
        {
            char[] separators = new char[] { ',' };
            return str.Trim().Replace(" ", "").Split(separators, StringSplitOptions.RemoveEmptyEntries);
        }


        ////////////////////////////////////////////////////////////////////////////////////////////////////
        // Проверка наличия нужной нам папки или создание её при возможности
        public bool CheckDir(string dir)
        {
            var result = false;
            try
            {
                if (Directory.Exists(dir)) result = true;
                else
                {
                    DirectoryInfo di = Directory.CreateDirectory(dir);
                    result = true;
                }
            }
            catch (Exception e)
            {
                Write_Log(MSG_DANGER, "CheckDir", e.ToString());
            }
            finally { }

            return result;
        }



        ////////////////////////////////////////////////////////////////////////////////////////////////////
        // Выведим строку с отладочной информацией
        public void Write_Log(int msg_type, string str1, string str2 = "")
        {
            string new_string = "";
            switch (msg_type)
            {
                case MSG_DANGER:
                    if (str2 == "") new_string = str1;
                    else new_string = string.Format(debug.MsgDanger, str1, str2);
                    break;
                case MSG_WARNING:
                    if (str2 == "") new_string = str1;
                    else new_string = string.Format(debug.MsgWarning, str1, str2);
                    break;
                case MSG_INFO:
                    if (str2 == "") new_string = str1;
                    else new_string = string.Format(debug.MsgInfo, str1, str2);
                    break;
                default:
                    if (str2 == "") new_string = str1;
                    else new_string = str1 + " " + str2;
                    break;
            }
            RutonyBot.SayToWindow(si.Name, new_string);
        }
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Объединение параметров для удобства
    public class Config
    {
        public string[] BotsList = { };
        public bool CheckAdmin = true;
        public string[] AdminList = { };
        public string[] BlackList = { };

        // Далее идут настройки, вынесенные в конфиг для удобства
        public string FolderData = "";
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Режим и уровень отлпдки для вывода информационных сообщений.
    public class DebugMode
    {
        public bool Mode = false;
        public int Level = 0;

        // Разное
        public string MsgDanger = "{0}: <span style='color:#FF0000;'>{1}</span>";
        public string MsgWarning = "{0}: <span style='color:#FFFF00;'>{1}</span>";
        public string MsgInfo = "{0}: <span style='color:#00FF00;'>{1}</span>";
    }


    ////////////////////////////////////////////////////////////////////////////////////////////////////
    // Информация по скрипту. Для удобства вынесена в отдельный класс.
    public class ScriptInfo
    {
        public string Info { get; private set; }
        public string Name { get; private set; }
        public string Version { get; private set; }
        public string Folder { get; private set; }
        public string Script { get; private set; }

        public ScriptInfo()
        {
            this.Info = "<span style='color:yellow;'>{0}</span> script, version <span style='color:#00FF00;'>{1}</span> {2}connected";
            this.Name = "Script Name";
            this.Version = "0.0";
            this.Folder = "auto_default";
            this.Script = "auto_default.cs";
        }

    }
}